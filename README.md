需要配置 MySQL 数据库
在 model 中找到 common.js 修改 mysql 配置数据连接到自己的数据库

```js
const connect = mysql.createConnection({
  host: 'localhost',
  port: 3306,
  user: '这里填自己MySQL的用户',
  password: '这里填自己MySQL的密码',
  database: '数据库名字'
})
```

需要三个表
article.sql:

```sql
DROP TABLE IF EXISTS article;`
`CREATE TABLE article (`
`_id int NOT NULL AUTO_INCREMENT,`
`title varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,`
`author varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,`
`publishDate timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,`
`cover varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL,`
`content varchar(10000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,`
`PRIMARY KEY (_id) USING BTREE,`
`INDEX author(author) USING BTREE,`
`CONSTRAINT author FOREIGN KEY (author) REFERENCES user (username) ON DELETE RESTRICT ON UPDATE RESTRICT`
`) ENGINE = InnoDB AUTO_INCREMENT = 10 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;`

`SET FOREIGN_KEY_CHECKS = 1;
```

comment.sql

```sql
DROP TABLE IF EXISTS `comment`;
CREATE TABLE `comment`  (
  `aid` int NOT NULL,
  `uid` int NOT NULL,
  `time` datetime NOT NULL,
  `content` varchar(10000) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `_id` int NOT NULL AUTO_INCREMENT,
  PRIMARY KEY (`_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 9 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
```

user.sql

```sql
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `_id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `email` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `role` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL,
  `state` int NOT NULL,
  PRIMARY KEY (`_id`) USING BTREE,
  INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 26 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
```

控制台输入 node .\server.js 启动项目
