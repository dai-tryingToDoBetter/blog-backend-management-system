const express = require('express')
const session = require('express-session')
const dateFormat = require('dateformat')
const template = require('art-template')
const home = require('./route/home')
const admin = require('./route/admin')
const bodyParser = require('body-parser')
const path = require('path')
const app = express()
app.use(express.static(path.join(__dirname, 'public')))
app.use(bodyParser.urlencoded({ extended: false }))
// 配置 session
app.use(
  session({
    secret: 'secret key',
    saveUninitialized: false,
    //cookie存在时间为一天
    cookie: {
      maxAge: 24 * 60 * 60 * 1000
    }
  })
)

// 拦截请求，判断用户登录状态
app.use('/admin', require('./middleware/loginGuard'))
app.use('/', home)
// 前台路由

app.use('/home', home)
// 后台路由
app.use('/admin', admin)
//集中处理错误请求
app.use((err, req, res, next) => {
  const result = JSON.parse(err)
  let params = []
  for (const attr in result) {
    if (attr != 'path') {
      params.push(attr + '=' + result[attr])
    }
  }
  res.redirect(`${result.path}?${params.join('&')}`)
})
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'art')
app.engine('art', require('express-art-template'))
// 向模板内部导入dateFormate变量
template.defaults.imports.dateFormat = dateFormat

app.listen('3000', () => {
  console.log('服务器启动成功')
})
