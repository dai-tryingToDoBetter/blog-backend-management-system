const mysql = require("mysql");

const connect = mysql.createConnection({
  host: "localhost",
  port: 3306,
  user: "root",
  password: "123456",
  database: "boke",
});
connect.connect();
/**
 *@function 获取MySQL数据库中的数函
 *@param  sql一条MySQL语句
 *@return Promise对象，异步函数
 @example await {result} = db("select * from user").then((result) => {
   return result;
 });
 **/
const db = (sql) =>
  new Promise((resolve, reject) =>
    connect.query(sql, (err, result) => (err ? reject(err) : resolve(result)))
  );

module.exports = { db };
