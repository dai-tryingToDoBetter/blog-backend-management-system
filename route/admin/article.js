const common = require("../../model/common");
module.exports = async (req, res) => {
  // 标识 标识当前访问的是文章页面，
  req.app.locals.currentLink = "article";
  //当前页数
  let page = req.query.page || 1;
  //每一页的数据条数
  let pagesize = 10;
  let { total, start } = await common
    .db("select * from article")
    .then((result) => {
      if (result.length <= 0)
        return res.render("./admin/article.art", { articles: {} });
      //数据库中数据个数
      let count = result.length;
      //总页数
      let total = Math.ceil(count / pagesize);
      let start = (page - 1) * pagesize;
      return { total: total, start: start };
    });
  //从MySQL数据库中限制获取到的数据，限制查询，需要多少查多少
  let sql = `select * from article limit ${start},${pagesize}`;
  //从数据库中拿到分页数据
  let articles = await common.db(sql).then((articles) => {
    return articles;
  });
  res.render("./admin/article.art", {
    articles: articles,
    page: page,
    total: total,
  });
};
