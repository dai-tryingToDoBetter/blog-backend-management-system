// 引入 joi 模块
const Joi = require("joi");
const common = require("../../model/common");

module.exports = (req, res, next) => {
  common
    .db(`select * from user where email='${req.body.email}'`)
    .then(async (result) => {
      if (result.length > 0) {
        return res.redirect(`/admin/user-edit?message=邮箱地址已经被占用`);
      } else {
        // 定义对象的验证规则
        const schema = {
          username: Joi.string()
            .alphanum()
            .min(2)
            .max(12)
            .required()
            .error(new Error("用户名不符合验证规则")),
          email: Joi.string()
            .email()
            .required()
            .error(new Error("邮箱格式不符合验证规则")),
          password: Joi.string()
            .regex(/^[a-zA-Z0-9]{3,30}$/)
            .required()
            .error(new Error("密码格式不符合验证规则")),
          role: Joi.string()
            .valid("normal", "admin")
            .required()
            .error(new Error("角色值非法")),
          state: Joi.number()
            .valid(0, 1)
            .required()
            .error(new Error("状态值非法")),
        };

        try {
          // 实施验证
          await Joi.validate(req.body, schema);
        } catch (err) {
          // 验证没有通过
          // 重定向回用户添加页面,可以将这一类错误用一个中间件集中处理
          // res.redirect('/admin/user-edit?message=' + err.message);
          let obj = { path: "/admin/user-edit", message: err.message };
          next(JSON.stringify(obj));
          return;
        }
        // 将新用户信息添加到数据库中
        common.db(
          `insert into user values(null,'${req.body.username}','${req.body.email}','${req.body.password}','${req.body.role}','${req.body.state}')`
        );
        // 将页面重定向到用户列表页
        res.redirect("/admin/user");
      }
    });
};
