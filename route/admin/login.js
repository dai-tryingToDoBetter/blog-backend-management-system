const express = require("express");
const common = require("../../model/common");
const login = express.Router();

login.post("/login", (req, res) => {
  const { email, password } = req.body;
  let sql = `select * from user where email='${email}'`;
  //判断邮件和密码是否为空
  if (email.trim().length == 0) {
    res.status(400).render("admin/error.art", { msg: "邮件地址或密码错误" });
  }
  //实现登录
  common.db(sql).then((result) => {
    if (result.length > 0) {
      result.forEach((v) => {
        if (password == v.password) {
          //将用户名存储到req请求中
          req.session.username = v.username;
          req.session.role = v.role;
          req.app.locals.userInfo = v;
          //判断用户是否是管理员，是就可以进入后台
          if (v.role == "admin") {
            res.redirect("/admin/user");
          } else {
            res.redirect("/home/");
          }
        } else {
          res
            .status(400)
            .render("admin/error.art", { msg: "邮件地址或密码错误" });
        }
      });
    } else {
      // 没有查询到用户
      res.status(400).render("admin/error.art", { msg: "邮件地址或密码错误" });
    }
  });
});

module.exports = login;
