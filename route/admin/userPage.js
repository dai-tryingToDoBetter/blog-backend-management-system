const common = require("../../model/common");

module.exports = async (req, res) => {
  // 标识 标识当前访问的是用户管理页面，
  req.app.locals.currentLink = "user";
  let page = req.query.page || 1;
  //每一页的数据条数
  let pagesize = 10;

  common.db("select * from user").then((result) => {
    let count = result.length;
    //总页数
    let total = Math.ceil(count / pagesize);
    let start = (page - 1) * pagesize;
    let sql = `select * from user limit ${start},${pagesize}`;
    common.db(sql).then((users) => {
      res.render("admin/user.art", {
        msg: req.session.username,
        users: users,
        page: page,
        total: total,
      });
    });
  });
};
