const common = require("../../model/common");
module.exports = (req, res) => {
  // 标识 标识当前访问的是用户管理页面，
  req.app.locals.currentLink = "user";
  const { message, id } = req.query;
  if (id) {
    common.db(`select * from user where _id=${id}`).then((user) => {
      user.forEach((v) => {
        //修改
        return res.render("admin/user-edit.art", {
          message: message,
          user: v,
          link: `/admin/user-modify?id=${id}`,
          button: "修改",
        });
      });
    });
  } else {
    //添加
    return res.render("admin/user-edit.art", {
      message: message,
      link: "/admin/user-edit",
      button: "添加",
    });
  }
};
