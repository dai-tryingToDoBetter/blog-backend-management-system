const formidable = require("formidable");
const path = require("path");
const common = require("../../model/common");
module.exports = (req, res) => {
  // 创建表单解析对象
  const form = new formidable.IncomingForm();
  // 配置上传文件的存放位置
  form.uploadDir = path.join(__dirname, "../", "../", "public", "uploads");
  // 保留上传文件的后缀
  form.options.keepExtensions = true;

  form.parse(req, (err, fields, files) => {
    //对文件名进行处理，因为'\'会杯MySQL数据库吞掉
    let str = files.cover._writeStream.path.split("public")[1];
    let newStr = str.slice(1, 8) + "/" + str.slice(9);
    //日期如果没有填就默认CURRENT_TIMESTAMP，数据库会自动处理
    let publishDate =
      fields.publishDate == "" ? "CURRENT_TIMESTAMP" : fields.publishDate;
    //写入
    if (publishDate == "CURRENT_TIMESTAMP") {
      common.db(
        `insert into article values(null,'${fields.title}','${fields.author}',${publishDate},'${newStr}','${fields.content}')`
      );
    } else {
      common.db(
        `insert into article values(null,'${fields.title}','${fields.author}','${publishDate}','${newStr}','${fields.content}')`
      );
    }
    // 将页面重定向到文章列表页面
    res.redirect("/admin/article");
  });
};
