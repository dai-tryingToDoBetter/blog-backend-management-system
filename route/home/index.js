const common = require("../../model/common");

module.exports = async (req, res) => {
  let page = req.query.page || 1;
  //每一页的数据条数
  let pagesize = 4;
  let { total = 0, start = 0 } = await common
    .db("select * from article")
    .then((result) => {
      if (result.length <= 0)
        return res.render("./admin/article.art", { articles: {} });
      let count = result.length;
      //总页数
      let total = Math.ceil(count / pagesize);
      let start = (page - 1) * pagesize;
      return { total: total, start: start };
    });
  let sql = `select * from article limit ${start},${pagesize}`;
  //从数据库中拿到分页数据
  let result = await common.db(sql).then((result) => {
    return result;
  });
  res.render("home/default.art", {
    result: result,
    page: page,
    total: total,
  });
};
