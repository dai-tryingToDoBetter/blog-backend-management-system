const common = require("../../model/common");

module.exports = async (req, res) => {
  let id = req.query.id;
  if (id == undefined) {
    return res.redirect("./");
  }
  //查找对应文章
  let article = await common
    .db(`select * from article where _id=${id}`)
    .then((article) => {
      return article;
    });
  //查找对应文章下的评论
  let comments = await common
    .db(`select * from comment where aid=${id}`)
    .then((comments) => {
      return comments;
    });
  for (let i = 0; i < comments.length; i++) {
    //通过评论的uid查找用户
    let user = await common
      .db(`select * from user where _id=${comments[i].uid}`)
      .then((user) => {
        return user;
      });
    for (let j = 0; j < user.length; j++) {
      comments[i].uid = user[j];
    }
  }

  res.render("home/article.art", {
    article: article[0],
    comments: comments,
  });
};
