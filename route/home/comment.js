const comment = require("../../model/common");

module.exports = (req, res) => {
  const { content, uid, aid } = req.body;
  if (content == "") return res.redirect("/home/article?id=" + aid);
  let date = new Date(); //返回当前的时间
  let time = `${date.getFullYear()}-${
    date.getMonth() + 1
  }-${date.getDate()} ${date.getHours()}:${date.getMinutes()}`;
  comment.db(
    `insert into comment values('${aid}','${uid}','${time}','${content}',null)`
  );
  //重定向到文章页面
  res.redirect("/home/article?id=" + aid);
};
